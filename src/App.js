import React from 'react'
import './App.css'
import './assets/css/theme.css'
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from 'react-router-dom'
import {
  createMuiTheme,
  ThemeProvider,
} from '@material-ui/core/styles'
import Login from './views/Login'
import Profile from './views/Profile'

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#a5d6a7',
      dark: '#75a478',
      light: '#d7ffd9',
    },
    secondary: {
      main: '#f48fb1',
      dark: '#bf5f82',
      light: '#ffc1e3',
    },
  },
})

class App extends React.Component {
  render() {
    const App = () => (
        <ThemeProvider theme={theme}>
          <section className="App">
            <Switch>
              <Route path="/" component={Login} />
              <Route path="/login" component={Login} />
              <Route path="/profile" component={Profile} />
              <Route render={() => <Redirect to="/login" />} />
            </Switch>
          </section>
        </ThemeProvider>
    )
    return (
        <Router>
          <Switch>
            <App />
          </Switch>
        </Router>
    )
  }
}

export default App

