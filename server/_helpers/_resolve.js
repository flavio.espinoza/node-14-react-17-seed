require('dotenv').config();

const log = require('ololog');
const isJson = require('is-json');
const path = require('path');

const writeMethod = async (_filename, _methodname, _line) => {
  let _fileInfo = path.parse(_filename);
  let _file = _fileInfo.base;
  return `${_file} -> line: ${_line} -> ${_methodname}`;
};

const logMethod = async (_filename, _methodname, _line) => {
  let _writeMethod = await writeMethod(_filename, _methodname, _line);
  console.log(_writeMethod);
};

const status = async (res, _filename, _methodname, _openid_provider, _line) => {
  let status_code = res.statusCode;
  let status_message = res.statusMessage;
  let success = status_code >= 200 && status_code <= 299;
  let method = await writeMethod(_filename, _methodname, _line);
  return {
    success: success,
    status_code: status_code,
    status_message: status_message,
    method: method,
    openid_provider: _openid_provider,
  };
};

const body = async (res_body) => {
  if (isJson(res_body)) {
    return JSON.parse(res_body);
  } else if (typeof res_body === 'string') {
    return res_body;
  } else if (!res_body) {
    return 'response.body is undefined';
  } else {
    let type = typeof res_body;
    return JSON.stringify({
      type: type,
      response: {
        body: res_body,
      },
    });
  }
};

module.exports = {
  status,
  body,
  writeMethod,
  logMethod,
};
