import React, { useState, useEffect } from 'react'
import { axios } from '../services/axios.service'
import ReactJson from 'react-json-view'
import _ from 'lodash'

const apiUrl = window.location.origin

const __getUserinfo = async (data) => {
  return axios({
    method: 'GET',
    url: `${apiUrl}/api/redirect`,
    params: {
      code: data.code,
      state: data.state,
      openid_provider: 'okta',
      oidc: 'okta',
    },
  })
}

function Redirect() {
  const [loading, setLoading] = useState(true)
  const [userinfo, setUserinfo] = useState({})
  
  useEffect(() => {
    (async () => {
      try {
        const urlParams = new URLSearchParams(window.location.search)
        const code = urlParams.get('code')
        const state = urlParams.get('state')
        const res = await __getUserinfo({
          code: code,
          state: state,
        })
        if (res.data && res.data.userinfo) {
          const body = JSON.parse(res.data.userinfo.body)
          console.log('body', body)
        }
      }
      catch (exception) {
        alert(`Error: ${exception.message}`)
      }
    })()
  }, [])
  
  return (
      <section className={'App view--profile'}>
        <h1>Profile</h1>
        {loading ? <div>Loading</div> : null}
        <i className="fad fa-hand-holding-seedling" />
        <ReactJson src={userinfo} />
      </section>
  )
}

export default Redirect