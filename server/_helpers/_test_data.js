require('dotenv').config();

const config = require('config');
const pd = config.get('pd_name');
const source_id = config.get('source_id');

const _ = require('lodash');
const _d = require('datedash');
const Chance = require('chance');
const chance = new Chance();
const crypto = require('crypto');

const build_test_subject = async () => {
  return new Promise(async (resolve) => {
    let birthDate = _d
      .date(chance.birthday({ string: true }), '-')
      .split('-')
      .reverse()
      .join('-');

    let givenName = chance.first({ nationality: 'en' });
    let familyName = chance.last({ nationality: 'en' });
    let fullName = `${givenName} ${familyName}`;
    let email = _.toLower(`${givenName}.${familyName}@pd.example.com`).replace(' ', '');
    let workEmail = _.toLower(`${givenName}.${familyName}@pd.work.example.com`).replace(' ', '');
    let otherEmail = _.toLower(`${givenName}.${familyName}@pd.other.example.com`).replace(' ', '');
    let mobilePhone = chance.phone();
    let postalCode = chance.zip();
    let social = `${givenName}${familyName}`;

    let id = crypto.createHash('md5').update(`${birthDate}:${email}`).digest('hex');

    const subject = {
      id,
      birthDate,
      fullName,
      givenName,
      familyName,
      email,
      workEmail,
      otherEmail,
      mobilePhone,
      social,
      postalCode,
    };
    resolve(subject);
  });
};

const build_subject_ids = async (sub, iss) => {
  let issuer_id = `${pd}/person/${iss.email}`;
  let subject_id = `${pd}/subject?id=${sub.id}`;
  let person_id = `${pd}/person?id=${sub.id}`;
  let email_id = `${pd}/person?email=${sub.email}`;
  let record_id = `ex_rid_${sub.id}`;
  let ingest_id = `${pd}/s/${source_id}/p/${record_id}`;
  return {
    issuer_id,
    subject_id,
    person_id,
    email_id,
    source_id,
    record_id,
    ingest_id,
  };
};

const build_ingest_reqest = {
  add_person: async (obj) => {
    let _sub = await build_test_subject();
    let _iss = {
      email: 'flavio@webshield.io',
    };
    const _id = await build_subject_ids(_sub, _iss);
    return {
      ado_data: {
        id: _id.ingest_id,
        issuer: _id.issuer_id,
        subjects: [
          {
            '@context': '@ingest/add-person',
            id: _id.subject_id,
            type: 'Person',
            sourceID: _id.source_id,
            recordID: _id.record_id,
            birthDate: _sub.birthDate,
            identifiers: {
              type: 'Person/Identifier',
              email: [
                {
                  type: 'Email',
                  value: _sub.email,
                },
              ],
              phoneNumber: [
                {
                  type: [
                    'PhoneNumber',
                    'PhoneNumber/Mobile',
                  ],
                  value: _sub.mobilePhone,
                  country: 'US',
                  domain: 'TMOBILE',
                  use: [
                    'PhoneNumber/Use/Work',
                    'PhoneNumber/Use/Home',
                    'PhoneNumber/Use/Mobile',
                  ],
                },
              ],
            },
            names: [
              {
                type: 'Name',
                givenName: _sub.givenName,
                familyName: _sub.familyName,
                fullName: _sub.fullName,
              },
            ],
            addresses: [
              {
                type: 'PostalAddress',
                streetAddress: `223 ${_sub.familyName} St`,
                addressLocality: 'Salt Lake City',
                addressRegion: 'UT',
                postalCode: _sub.postalCode,
                addressCountry: 'US',
                fullAddress: `223 ${_sub.familyName} St, Salt Lake City, UT, ${_sub.postalCode}, US`,
              },
            ],
          },
        ],
      },
    };
  },
};

const dept = [
  'Agriculture Weights and Measures',
  'Air Pollution Control District',
  'Aging and Independence Services',
  'Animal Services',
  'Assessor Recorder County Clerk',
  'Auditor Controller',
  'Behavioral Health Services',
  'Chief Administrative Office',
  'Child and Family Strengthening Advisory Board',
  'Child Support Services',
  'Child Welfare Services',
  'Citizens Law Enforcement Review Board',
  'Civil Service Commission',
  'Clerk of the Board of Supervisors',
  'Communications Office',
  'County Counsel',
  'District Attorney',
  'Diversity and Inclusion',
  'Emergency Services',
  'Environmental Health',
  'Ethics and Compliance',
  'Finance and General Government Group',
  'Fire Authority',
  'General Services',
  'Grand Jury',
  'Health and Human Services Agency',
  'Housing and Community Development',
  'Human Resources',
  'Land Use and Environment Group',
  'Library',
  'Medical Examiner',
  'Parks and Recreation',
  'Planning and Development Services',
  'Probation',
  'Public Administrator Guardian and Conservator',
  'Public Defender',
  'Public Health',
  'Public Safety Group',
  'Public Works',
  'Purchasing and Contracting',
  'Registrar of Voters',
  'Retirement Association SDCERA',
  'SanGIS Maps',
  'Sheriff',
  'Strategy and Intergovernmental Affairs',
  'Technology Office',
  'Treasurer Tax Collector',
  'UC Cooperative Extension',
];

const build_form_data = {
  add_person: async () => {
    let _sub = await build_test_subject();
    let _ssn = chance.ssn();
    let split = _ssn.split('-');
    let _last4 = split[2];
    return {
      first_name: {
        value: _sub.givenName,
      },
      middle_name: {
        value: 'F',
      },
      last_name: {
        value: _sub.familyName,
      },
      honorific_prefix: {
        value: chance.prefix(),
      },
      honorific_suffix: {
        value: chance.suffix(),
      },
      mobile_phone: {
        value: _sub.mobilePhone,
        use_work: false,
        use_home: false,
        use_mobile: true,
        use_fax: false,
        use_pager: false,
        use_other: false,
      },
      home_phone: {
        value: _sub.mobilePhone,
        use_work: false,
        use_home: true,
        use_mobile: false,
        use_fax: false,
        use_pager: false,
        use_other: false,
      },
      work_phone: {
        value: _sub.mobilePhone,
        use_work: true,
        use_home: false,
        use_mobile: false,
        use_fax: false,
        use_pager: false,
        use_other: false,
      },
      fax_number: {
        value: `(650) 297-3312`,
        use_work: false,
        use_home: false,
        use_mobile: false,
        use_fax: true,
        use_pager: false,
        use_other: false,
      },
      pager_number: {
        value: _sub.mobilePhone,
        use_work: false,
        use_home: false,
        use_mobile: false,
        use_fax: false,
        use_pager: true,
        use_other: false,
      },
      other_number: {
        value: `(650) 695-6911`,
        use_work: false,
        use_home: false,
        use_mobile: false,
        use_fax: false,
        use_pager: false,
        use_other: true,
      },
      email: {
        value: _sub.email,
      },
      work_email: {
        value: _sub.workEmail,
      },
      second_email: {
        value: _sub.otherEmail,
      },
      birth_date: {
        value: _sub.birthDate,
      },
      gender: {
        value: null,
      },
      facebook: {
        value: _sub.social,
      },
      twitter: {
        value: _sub.social,
      },
      instagram: {
        value: _sub.social,
      },
      street_address: {
        value: `223 ${_sub.familyName} St`,
      },
      city: {
        value: 'Salt Lake City',
      },
      state: {
        value: 'UT',
      },
      zip_code: {
        value: '84115',
      },
      country_code: {
        value: 'US',
      },
      drivers_licence: {
        value: chance.bb_pin(),
        domain: 'UT',
      },
      passport_number: {
        value: chance.bb_pin(),
        domain: 'US',
      },
      ssn: {
        value: _ssn,
      },
      ssn_last4: {
        value: _last4,
      },
      tax_id: {
        value: chance.cpf(),
      },
      other_id: {
        value: `oid-${chance.bb_pin()}`,
      },
      account_number: {
        value: `account-${chance.bb_pin()}`,
      },
    };
  },
  add_org: async () => {
    let _company = chance.company();
    let _web = `${_.toLower(_company.replace('&', 'and').replace(/,/g, '').replace(/\'/g, '').replace(/\./g, '').replace(/\s/g, '-'))}`;
    let _social = `${_company.replace('&', 'and').replace(/,/g, '').replace(/\'/g, '').replace(/\./g, '').replace(/\s/g, '')}`;
    let _dns = `${_web}.com`;    
    return {
      legal_name: {
        value: _company,
      },
      name: {
        value: _company,
      },
      description: {
        value: `We make epic stuff!`,
      },
      categorization_tag: {
        value: `Work`,
      },
      is_department_of: {
        value: null,
      },
      website: {
        value: _dns,
      },
      phone_number: {
        value: chance.phone(),
        use_work: true,
        use_home: true,
        use_mobile: true,
        use_fax: false,
        use_pager: false,
        use_other: false,
      },
      fax_number: {
        value: `(650) 297-3312`,
        use_work: false,
        use_home: false,
        use_mobile: false,
        use_fax: true,
        use_pager: false,
        use_other: false,
      },
      email: {
        value: `info@${_dns}`,
      },
      second_email: {
        value: `estimates@${_dns}`,
      },
      facebook: {
        value: _social,
      },
      twitter: {
        value: _social,
      },
      instagram: {
        value: _social,
      },
      street_address: {
        value: chance.address({short_suffix: true}),
      },
      city: {
        value: `Salt Lake City`,
      },
      state: {
        value: `UT`,
      },
      zip_code: {
        value: `84115`,
      },
      country_code: {
        value: `US`,
      },
      tax_id_number: {
        value: chance.cpf(),
      },
      tax_id_country: {
        value: `US`,
      },
      duns: {
        value: chance.cpf(),
      },
      departments: {
        value: []
      },
    };
  },
};

module.exports = {
  build_form_data,
};

let test = false;

if (test) {
  (async function () {
    try {
      const form_data = await build_form_data.add_org();
      console.log(`line ${__line}: form_data`, form_data);
    } catch (err) {
      console.error(err);
    }
  })();
}
