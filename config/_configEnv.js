require('dotenv').config()

const axios = require('axios')
const jsYaml = require('js-yaml')
const _error = require('../server/_helpers/_error')._error
const fs = require('fs')
const path = require('path')
const _ = require('lodash')
const log = require('ololog')
const writeJson = require('write-json')
const flatten = require('flat')
const unflatten = require('flat').unflatten
const oktaWellKnown = require('./okta.well-known.json');

async function asyncEach(arr, asyncCallbackFn) {
  for (let i = 0; i < arr.length; i++) {
    await asyncCallbackFn(arr[i], i, arr)
  }
}

const _getOktaWellKnown = async () => {
  return oktaWellKnown;
}

const _configEnv = async (configPath) => {
  try {
    const res = await _getOktaWellKnown()
    const okta_well_known = res.data
    const filePath = path.isAbsolute(configPath)
      ? configPath
      : path.resolve(__dirname, configPath)
    const ymlConfig = jsYaml.safeLoad(fs.readFileSync(filePath), 'utf8')
    const config_local = ymlConfig['exemplar']
    const config_openid = {
      okta: {
        openid_provider: 'okta',
        ...okta_well_known,
      },
    }
    const config_flat = {
      ...flatten(config_local),
      ...flatten(config_openid),
    }
    const _keys = _.keys(config_flat)
    async function _resolve(env_var) {
      return new Promise(async (resolve) => {
        let result
        try {
          result = eval(env_var)
          if (!result) {
            resolve(undefined)
          } else {
            resolve(result)
          }
        } catch (err) {
          if (!env_var) {
            resolve(undefined)
          } else {
            result = eval('`' + env_var + '`')
            resolve(result)
          }
        }
      })
    }
    async function _eval() {
      let result = {}
      await asyncEach(_keys, async (key) => {
        const envVarValue = await _resolve(config_flat[key])
        if (envVarValue === 'undefined') {
          _error({
            type: 'config_create_warning',
            file: '_configEnv.js',
            method: '_eval()',
            error: {
              message: `WARNING: config env_var: ${config_flat[key]} is undefined`,
            },
          })
        } else {
          result[key] = envVarValue
        }
      })
      return result
    }
    const _config_env_flat = await _eval()
    const _config_env = unflatten(_config_env_flat)
    await writeJson(__dirname + '/local.json', _config_env)
    return _config_env
  } catch (err) {
    _error({
      type: 'config_create_catch_error',
      file: '_configEnv.js',
      method: '_configEnv()',
      error: err,
    })
  }
}

/**
 * Immediately Invoked Function Expression (IIFE)
 * */
;(async function () {
  try {
    await _configEnv('./ex_config.yml')
  } catch (err) {
    _error({
      type: 'config_create_error',
      file: '_configEnv.js',
      method: 'IIFE ;(async function() {})()',
      error: err,
    })
  }
})()
