const log = require('ololog');
const _ = require('lodash');

const date_diff_days = (start, end) => {
  const _MS_PER_DAY = 1000 * 60 * 60 * 24;
  const utc1 = Date.UTC(start.getFullYear(), start.getMonth(), start.getDate());
  const utc2 = Date.UTC(end.getFullYear(), end.getMonth(), end.getDate());
  return Math.floor((utc2 - utc1) / _MS_PER_DAY);
};

const calc_duration = async ({ startDate, endDate }) => {
  const start = new Date(startDate);
  const end = endDate ? new Date(endDate) : new Date();
  const days = date_diff_days(start, end);
  log.yellow('days ------> ', days);
  return _.toString(days);
};

const utc_date = (date) => {
  const dateObj = new Date(date);
  return dateObj.toJSON().substring(0, 10);
};

const build_full_name = (userinfo) => {
  if (userinfo.middle_name) {
    return `${userinfo.given_name} ${userinfo.middle_name} ${userinfo.family_name}`;
  } else {
    return `${userinfo.given_name} ${userinfo.family_name}`;
  }
};

/**
* Test: local dev testing
*/
let test = true;
if (test) {
  let date = new Date();
  console.log(date);
  // => Wed Apr 29 2020 23:27:01 GMT-0600 (Mountain Daylight Time)

  let date_utc = utc_date(date);
  console.log(date_utc);
  // => 2020-04-30
}

/**
* Exports
*/
module.exports = {
  calc_duration,
  utc_date,
  build_full_name,
};
