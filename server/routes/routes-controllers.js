require('dotenv').config()

const config = require('config')
const jwt = require('jsonwebtoken')
const chalk = require('chalk')
const _ = require('lodash')
const openid = require('../openid/openid-client')

module.exports.register = async (ctx, next) => {
  const oidc = config.get('okta')
  ctx.body = {
    registration_endpoint: oidc['uri_register'],
  }
  await next()
}

module.exports.login = async (ctx, next) => {
  ctx.cookies.set('ACCESS_TOKEN', null)
  ctx.cookies.set('ID_TOKEN', null)
  ctx.cookies.set('REFRESH_TOKEN', null)
  const get_auth_url = await openid.get_auth_url()
  ctx.body = {
    ...get_auth_url,
  }
  await next()
}

module.exports.redirect = async (ctx, next) => {
  const query = ctx.query
  const code = query.code
  const openid_provider = query.openid_provider
  const oidc = {
    config: config.get(openid_provider),
  }
  const client_id = oidc.config['client_id']
  const client_secret = oidc.config['client_secret']
  const redirect_uri = oidc.config['uri_redirect']
  const token_endpoint = oidc.config['token_endpoint']
  const get_iat_exp = () => {
    let hr_ms = 3.6e6
    let iaf = new Date().getTime()
    let exf = iaf + hr_ms
    return {
      okta: {
        iat: Math.floor(iaf / 1000),
        exp: Math.floor(exf / 1000),
      },
      iat: iaf,
      exp: exf,
    }
  }
  const exp = get_iat_exp()[oidc.config.provider].exp
  const client_secret_jwt_claims = {
    aud: token_endpoint,
    exp: exp,
    iss: client_id,
    sub: client_id,
  }
  const client_secret_jwt = jwt.sign(client_secret_jwt_claims, client_secret, {
    algorithm: 'HS256',
  })
  const jwt_params = {
    grant_type: 'authorization_code',
    code: code,
    redirect_uri: redirect_uri,
    client_assertion_type: 'urn:ietf:params:oauth:client-assertion-type:jwt-bearer',
    client_assertion: client_secret_jwt,
  }
  const openid_token = await openid.token(query, jwt_params)
  if (openid_token.body.id_token && openid_token.body.access_token) {
    const id_token = openid_token.body.id_token
    const access_token = openid_token.body.access_token
    const id_decoded = jwt.decode(id_token, client_secret)
    const userinfo = await openid.userinfo(access_token)
    const auth_user = {
      userID: id_decoded.sub,
      email: id_decoded.email,
    }
    ctx.cookies.set('ID_TOKEN', id_token)
    ctx.cookies.set('ACCESS_TOKEN', access_token)
    ctx.body = {
      success: true,
      userinfo: userinfo,
      message: `${openid_provider} id_token and access_token granted`,
      user: auth_user,
      redirect_url: oidc.config['uri_redirect_authenticated'],
    }
    await next()
  } else {
    ctx.cookies.set('ID_TOKEN', null)
    ctx.cookies.set('ACCESS_TOKEN', null)
    ctx.body = {
      success: false,
      message: `${openid_provider} id_token and access_token failure`,
      response: openid_token,
      redirect_url: oidc.config['uri_redirect_login'],
    }
    await next()
  }
}

module.exports.logout = async (ctx, next) => {
  const logout = await openid.logout(ctx)
  ctx.cookies.set('ACCESS_TOKEN', null)
  ctx.cookies.set('ID_TOKEN', null)
  ctx.body = logout
  await next()
}

module.exports.profile = async (ctx, next) => {
  const access_token = ctx.cookies.get('ACCESS_TOKEN')
  const res = await openid.userinfo(access_token)
  ctx.body = {
    ...res,
  }
  await next()
}

module.exports.openid = openid
