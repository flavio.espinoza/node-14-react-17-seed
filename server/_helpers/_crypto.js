require('dotenv').config();

const config = require('config');
const app_secret_1 = config.get('app_secret_1');
const app_secret_2 = config.get('app_secret_2');

const _ = require('lodash');
const crypto = require('crypto');
const algorithm = 'aes-256-cbc';

const key = crypto.scryptSync(app_secret_1, 'salt', 32);
const iv = crypto.scryptSync(app_secret_2, 'salt', 16);

function encrypt(text) {
  let cipher = crypto.createCipheriv(algorithm, Buffer.from(key), iv);
  let encrypted = cipher.update(text);
  encrypted = Buffer.concat([encrypted, cipher.final()]);
  return { iv: iv.toString('hex'), encryptedData: encrypted.toString('hex') };
}

function decrypt(text) {
  // @fix : decrypt tokens
  let iv = Buffer.from(text.iv, 'hex');
  let encryptedText = Buffer.from(text.encryptedData, 'hex');
  let decipher = crypto.createDecipheriv(algorithm, Buffer.from(key), iv);
  let decrypted = decipher.update(encryptedText);
  decrypted = Buffer.concat([decrypted, decipher.final()]);
  return decrypted.toString();
}

// Exports
module.exports = {
  encrypt,
  decrypt,
};

// Test
let test = false;
if (test) {
  let id_token = 'eyJraWQiOiJ1WC0wVkFCYWFHRGhOTk9Iem1tdWZVVUZkT1lzUGZ5d2lTZzRFcDZFaTIwIiwiYWxnIjoiUlMyNTYifQ';
  let access_token = 'eyJraWQiOiJ6ZXdTQkdxVkJneU5sSUZPR3ZPQVpMMVNTdC1mV0h6NHJzSlhWeEJJOFU4IiwiYWxnIjoiUlMyNTYifQ';

  let id = encrypt(id_token);
  console.log(id);
  console.log(decrypt(id));

  let access = encrypt(access_token);
  console.log(access);
  console.log(decrypt(access));
}
