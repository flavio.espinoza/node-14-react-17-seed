import React, { useState, useEffect } from 'react'
import Paper from '@material-ui/core/Paper'
import Avatar from '@material-ui/core/Avatar'
import Button from '@material-ui/core/Button'
import CssBaseline from '@material-ui/core/CssBaseline'
import Link from '@material-ui/core/Link'
import Grid from '@material-ui/core/Grid'
import Box from '@material-ui/core/Box'
import Typography from '@material-ui/core/Typography'
import { makeStyles } from '@material-ui/core/styles'
import Container from '@material-ui/core/Container'
import { axios } from '../services/axios.service'
import ReactJson from 'react-json-view'

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: theme.spacing(4),
  },
  avatar: {
    margin: theme.spacing(2),
    backgroundColor: theme.palette.secondary.main,
  },
  link: {
    margin: theme.spacing(1),
    color: theme.palette.secondary.dark,
  },
  form: {
    width: '100%',
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}))

const apiUrl = window.location.origin

const __getProfile = async (data) => {
  return axios({
    method: 'GET',
    url: `${apiUrl}/api/redirect`,
    params: {
      code: data.code,
      state: data.state,
      openid_provider: 'okta',
      oidc: 'okta',
    },
  })
}

function Profile() {
  const [profile, setProfile] = useState({})
  const classes = useStyles()
  useEffect(() => {
    (async () => {
      try {
        const urlParams = new URLSearchParams(window.location.search)
        const code = urlParams.get('code')
        const state = urlParams.get('state')
        const res = await __getProfile({
          code: code,
          state: state,
        })
        if (res.data && res.data.userinfo) {
          const body = JSON.parse(res.data.userinfo.body)
          setProfile(body)
          console.log('body', body)
          sessionStorage.setItem('email', body.email)
        } else {
          window.location.href = '/login'
        }
      }
      catch (exception) {
        alert(`Error: ${exception.message}`)
      }
    })()
  }, [])
  
  return (
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <Paper elevation={3} className={classes.paper}>
          <Avatar className={classes.avatar}>
            <i className="fad fa-user" /> </Avatar>
          {profile.email ?
              <Typography component="h1" variant="h5" className={'mb-2'}>
                {profile.email}
              </Typography> : null}
          <div className={'p-2'}>
            <ReactJson style={{
              padding: 12,
            }} src={profile} theme={'monokai'} />
          </div>
        </Paper>
      </Container>
  )
}

export default Profile