import axios from 'axios'

const apiUrl = process.env.API_URL
const refresh_token = null

async function logout() {
  // Remove user from store
}

async function updateRequestConfig(config, accessToken) {
  config.headers.Authorization = `Bearer ${accessToken}`
  config.__isRetry = true
  return config
}

async function getNewAccessToken(token) {
  return axios({
    method: 'POST',
    url: `${apiUrl}/auth/v1/token-refresh/`,
    data: {
      refresh: token,
    },
    headers: { 'Content-type': 'application/json' },
  })
}

axios.interceptors.response.use(
    (res) => res,
    async (error) => {
      // if the error.response.status is 401 Unauthorized
      // request a new access_token
      if (error.response.status === 401 && !error.config.__isRetry) {
        try {
          const res = await getNewAccessToken(refresh_token)
          const access_token = res.data.token
          const updatedConfig = await updateRequestConfig(error.config, access_token)
          return axios(updatedConfig)
        }
        catch (error) {
          console.log(error)
          // this exception is automatically handled by the ExceptionHandler
        }
      }
      throw new ExceptionHandler(error)
    },
)

function ExceptionHandler(error) {
  this.is_error = true
  this.error = error
}

export { axios, ExceptionHandler }
