require('dotenv').config()

const log = require('ololog')
const path = require('path')
const Koa = require('koa')
const logger = require('koa-logger')
const cors = require('kcors')
const bodyParser = require('koa-bodyparser')
const serve = require('koa-static')
const send = require('koa-send')
const Router = require('@koa/router')
const router = new Router()

const PORT_SERVER = process.env.PORT_SERVER

const {
  register,
  login,
  redirect,
  logout,
  profile,
} = require('./routes/routes-controllers')

router.post('/api/register', register)
router.post('/api/login', login)
router.post('/api/logout', logout)
router.get('/api/redirect', redirect)
router.get('/api/profile', profile)

const root = path.parse(__dirname).dir
const app = new Koa()
app.proxy = true
app.use(serve(root + '/build'))
app.use(async function(ctx, next) {
  return send(ctx, '/build/index.html', { root: root, index: 'index.html' }).
  then(() => next())
})
app.use(cors())
app.use(logger())
app.use(bodyParser())
app.use(router.routes())
app.listen(PORT_SERVER, () => log.blue(
    `Backend server listening on port: ${PORT_SERVER}`))

process.on('unhandledRejection', (reason, promise) => {
  log.lightYellow('Unhandled Rejection at:', promise, 'reason:', reason)
})
