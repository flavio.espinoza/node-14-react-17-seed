require('dotenv').config();

const log = require('ololog');
const _ = require('lodash');
const writeJson = require('write-json');
const flatten = require('flat');
const unflatten = require('flat').unflatten;


async function _eval(res_json) {

  let _flatten = {
    ...flatten(res_json),
  };

  let _keys = _.keys(_flatten);

  async function asyncEach(arr, callback) {
    for (let i = 0; i < arr.length; i++) {
      await callback(arr[i], i, arr);
    }
  }

  async function _config(env_var) {
    return new Promise(async (resolve) => {
      try {
        let result = eval(env_var);
        if (!result) {
          resolve();
        } else {
          resolve(result);
        }
      } catch (err) {
        let result = eval('`' + env_var + '`');
        resolve(result);
      }
    });
  }

  async function _iterate() {
    const result = {};
    await asyncEach(_keys, async (key) => {
      result[key] = await _config(_flatten[key]);
    });
    return result;
  }
}

module.exports = _eval;

(async function() {
  try {
    const _config_env_flat = await _iterate();
    const _config_env = unflatten(_config_env_flat);
    await writeJson(__dirname + '/_config_env_flat.json', _config_env_flat);
    await writeJson(__dirname + '/_config_env.json', _config_env);

  } catch (err) {
    log.lightYellow('_eval.js', err.message);
  }
})();
