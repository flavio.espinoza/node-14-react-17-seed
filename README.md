# node-14-react-17-seed

Seed project built on Node v14 TLS and React 17

## Prerequisites

You will need to set up a free developer account with Okta.

- [Okta Developer Platform](https://developer.okta.com/)

Next you will create an Identity Provider and Register an OpenID App.

- [Create an Identity Provider in Okta](https://developer.okta.com/docs/guides/add-an-external-idp/openidconnect/configure-idp-in-okta/)

You will need values from the OpenID app in the [Environment Variables](#environment-variables) section of this document.

**IMPORTANT**: You need to add the `Login redirect URIs` and `Initiate login URI` exactly as they are shown in the image below:

![okta-openid-app](docs/img/okta-openid-app.png)

## Getting Started

Clone repository:

```bash
git clone git@gitlab.com:flavio.espinoza/node-14-react-17-seed.git
```

CD into local project directory:

```bash
cd node-14-react-17-seed
```

Install dependencies:

```bash
yarn install
```

## Environment Variables

Create a `.env` file in project root:

```bash
touch .env
```

Copy and paste the following content into your local `.env` file:

```bash
NODE_ENV=development
NODE_PATH=./

# Node backend server port
PORT_SERVER=5000

# React frontend server port
PORT=8080

# OpenID authentication and authorization credentials
OKTA_ISSUER="<your_domain>.okta.com"
OKTA_CLIENT_ID="<your_okta_client_id>"
OKTA_CLIENT_SECRET="<your_okta_client_secret>"
```

In the `.env` file, replace the `OKTA_ISSUER`, `OKTA_CLIENT_ID` and `OKTA_CLIENT_SECRET` with the values from your Okta OpenID application:

```bash

# OpenID authentication and authorization credentials
OKTA_ISSUER="https://dev-00001.okta.com"
OKTA_CLIENT_ID="0oa1i7ei2iX..."
OKTA_CLIENT_SECRET="-fnhLpcl99-wsxTS..."
```

## Config

Build local config JSON file:

```bash
yarn config-all
```

## Build

Build app for local development:

```bash
yarn build
```

Builds the app as it would for production to the `build` folder which is where the node server serves up static frontend files to the browser.

It bundles React in a mode and optimizes the frontend for the best performance.

The build is minified and the filenames include the hashes.

## Run

Start the Node `backend` development `server`:

```bash
yarn server
```

Open a second terminal and start the React `frontend` development `client`:

```bash
yarn client
```

Runs the React frontend app in the development mode. The app will reload if you make edits.

## Browser

Open [http://localhost:8080](http://localhost:8080) to view it in the browser.

Click the `Login` button:

![login](docs/img/login.png)

You may be prompted to `Sign In` or click the `Sign Up` link to register as a new user:

![okta-login-signup](docs/img/okta-login-signup.png)

Once you have authenticated with Okta you will be redirected back to the app and see your Okta userinfo as a JSON:

![profile](docs/img/profile.png)

Refresh the profile page to return to the login screen:

## Test

```bash
yarn test
```

Launches the test runner in the interactive watch mode.

See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.
