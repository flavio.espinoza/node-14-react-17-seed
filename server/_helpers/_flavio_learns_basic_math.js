function dollarsToCents(amount) {
  if (Number.isInteger(amount)) {
    return amount;
  }
  return Math.round(parseFloat(amount) * 100);
}

function amountToCoins(amount, coins) {
  let remainder;
  if (amount === 0) {
    return [];
  } else {
    let cents = dollarsToCents(amount);
    if (cents >= coins[0]) {
      remainder = cents - coins[0];
      return [coins[0]].concat(amountToCoins(remainder, coins));
    } else {
      coins.shift();
      return amountToCoins(cents, coins);
    }
  }
}

console.log(amountToCoins(1.21, [25, 10, 5, 1]));

// output => [ 25, 25, 25, 25, 10, 10,  1 ]
