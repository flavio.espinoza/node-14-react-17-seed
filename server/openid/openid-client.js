require('dotenv').config()

const config = require('config')
const chalk = require('chalk')
const request = require('request')
const { v4: uuid } = require('uuid')

const isJson = require('is-json')
const _config = require('../../config/local.json')
const _error = require('../_helpers/_error')

const oidc = {
  config: config.get('okta'),
}

const authz_header = ({ client_id, client_secret }) => {
  const Basic = 'Basic ' +
      Buffer.from(`${client_id}:${client_secret}`).toString('base64')
  return {
    Accept: 'application/json',
    'Content-Type': 'application/x-www-form-urlencoded',
    authorization: Basic,
  }
}

module.exports.make_error_handler = (emitter) => (err) => {
  if (err.type) {
    emitter.emit('error', `${err.type} - ${err.text}`)
  } else {
    emitter.emit('error', err)
  }
}

module.exports.get_iat_exp = () => {
  let hr_ms = 3.6e6
  let iaf = new Date().getTime()
  let exf = iaf + hr_ms
  return {
    okta: {
      iat: Math.floor(iaf / 1000),
      exp: Math.floor(exf / 1000),
    },
    iat: iaf,
    exp: exf,
  }
}

module.exports.resolve_query = async (query) => {
  let result = {}
  if (query.state) {
    let split = query.state.split('___')
    result = {
      ...query,
      openid_provider: split[0],
    }
    return result
  } else {
    return query
  }
}

module.exports.resolve_config = async (provider) => {
  return new Promise(async (resolve) => {
    resolve(oidc['okta'])
  })
}

module.exports.get_config = async (query, from_method) => {
  const _creds = _config['okta']
  return {
    openid_provider: 'okta',
    ..._creds,
  }
}

module.exports.get_auth_params = async (query) => {
  console.log(chalk.bgRed(JSON.stringify(query, null, 2)))
  return new Promise(async (resolve) => {
    const openid_provider = query.openid_provider
    const openid_config = config.get(openid_provider)
    const _state = `${openid_provider}___${uuid()}`
    const _nonce = new Date().getTime()
    const _scope = 'openid email phone profile'
    const _params = {
      response_type: 'code',
      client_id: openid_config['client_id'],
      redirect_uri: openid_config['uri_redirect'],
      scope: _scope,
      state: _state,
      nonce: _nonce,
    }
    const result = {
      openid_provider: openid_provider,
      auth_uri: openid_config['authorization_endpoint'],
      params: _params,
    }
    resolve(result)
  })
}

module.exports.get_auth_url = async () => {
  const oidc = config.get('okta')
  const _state = `${'okta'}___${uuid()}`
  const _nonce = new Date().getTime()
  const _scope = 'openid email phone profile'
  const params = {
    response_type: 'code',
    client_id: oidc['client_id'],
    redirect_uri: oidc['uri_redirect'],
    scope: _scope,
    state: _state,
    nonce: _nonce,
  }
  return new Promise(async (resolve) => {
    let params_str = Object.entries(params).
    map((arr) => `${arr[0]}=${arr[1]}`).
    join('&')
    let authenticate_url = `${oidc['authorization_endpoint']}?${params_str}`
    resolve({
      message: 'openid.get_auth_url',
      params: params,
      authenticate_url: authenticate_url,
      openid_provider: 'okta',
    })
  })
}

module.exports.get_auth_method = async (query, jwt_params) => {
  return new Promise((resolve) => {
    const openid_provider = 'okta'
    const _creds = _config[openid_provider]
    const client_id = _creds['client_id']
    const client_secret = _creds['client_secret']
    const token_endpoint = _creds['token_endpoint']
    const redirect_uri = _creds['uri_redirect']
    const code = query.code
    const state = query.state
    const client_secret_basic_params = {
      client_id: client_id,
      grant_type: 'authorization_code',
      code: code,
      redirect_uri: redirect_uri,
      state: state,
      client_secret: client_secret,
    }
    const client_secret_basic = {
      method: 'POST',
      url: token_endpoint,
      qs: client_secret_basic_params,
      headers: authz_header({ client_id, client_secret }),
    }
    const client_secret_jwt = {
      method: 'POST',
      url: token_endpoint,
      qs: jwt_params,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
    }
    if (openid_provider === 'okta') {
      resolve({
        name: 'client_secret_jwt',
        options: client_secret_jwt,
        openid_provider: openid_provider,
      })
    } else {
      if (openid_provider === 'pingfederate' || openid_provider === 'pingone') {
        resolve({
          name: 'client_secret_basic',
          options: client_secret_basic,
          openid_provider: openid_provider,
        })
      }
    }
  })
}

module.exports.token = async (query, jwt_params) => {
  return new Promise(async (resolve) => {
    const auth_method = await this.get_auth_method(query, jwt_params)
    const options = auth_method.options
    const openid_provider = auth_method.openid_provider
    request(options, async (err, response, body) => {
      if (err) {
        let error = {
          type: 'config_create_error',
          file: '_configEnv.js',
          method: '_configEnv(_docker.config)',
          error: err,
        }
        _error(error)
        resolve({
          status_code: response.statusCode,
          success: false,
          error: error,
          openid_provider: openid_provider,
          response_headers: response.headers,
        })
      }
      let _body = isJson(body) ? JSON.parse(body) : body
      const obj = {
        status_code: response.statusCode,
        success: true,
        body: _body,
        openid_provider: openid_provider,
        response_headers: response.headers,
      }
      resolve(obj)
    })
  })
}

module.exports.userinfo = async (access_token) => {
  const _config = oidc.config
  const userinfo_endpoint = _config['userinfo_endpoint']
  const options = {
    method: 'GET',
    url: userinfo_endpoint,
    headers: {
      Authorization: `Bearer ${access_token}`,
    },
  }
  return new Promise(async (resolve) => {
    request(options, function(error, response, body) {
      resolve(response)
    })
  })
}

module.exports.validate_token = async (token, token_type_hint) => {
  return new Promise(async (resolve) => {
    const client_id = oidc.config.client_id
    const client_secret = oidc.config.client_secret
    const introspection_endpoint = oidc.config.introspection_endpoint
    const options = {
      method: 'POST',
      url: introspection_endpoint,
      qs: {
        token: token,
        token_type_hint: token_type_hint,
      },
      headers: authz_header({ client_id, client_secret }),
    }
    request(options, async (error, response, body) => {
      let _body = isJson(body) ? JSON.parse(body) : body
      resolve(_body)
    })
  })
}

module.exports.logout = async (ctx) => {
  const id_token = ctx.cookies.get('ID_TOKEN')
  const _config = oidc.config
  const end_session_endpoint = _config['end_session_endpoint']
  const post_logout_redirect_uri = _config['uri_redirect_logout']
  const state = `${_config['openid_provider']}___${uuid()}`
  const logout_url = `${end_session_endpoint}?id_token_hint=${id_token}&post_logout_redirect_uri=${post_logout_redirect_uri}&state=${state}`
  return {
    logout_url: logout_url,
  }
}
