const log = require('ololog');

module.exports._error = ({ type, file, method, error, info }) => {
  const result = {
    type: type || 'error_type_not_provided',
    file: file || 'file_not_provided',
    method: method,
    error: error,
    info: info,
  };
  log.lightYellow(JSON.stringify(result, null, 2));
  return result;
};
