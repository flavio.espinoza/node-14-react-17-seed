import React, { useState, useEffect } from "react";
import Paper from "@material-ui/core/Paper";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import Link from "@material-ui/core/Link";
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import { axios } from "../services/axios.service";

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {"Copyright © "}
      <Link
        color="inherit"
        href="https://gitlab.com/flavio.espinoza/node-14-react-17-seed"
      >
        Flavio Espinoza
      </Link>{" "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    padding: theme.spacing(4),
  },
  avatar: {
    margin: theme.spacing(2),
    backgroundColor: theme.palette.secondary.main,
  },
  link: {
    margin: theme.spacing(1),
    color: theme.palette.secondary.dark,
  },
  form: {
    width: "100%",
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

const apiUrl = window.location.origin;

const __login = async () => {
  return axios({
    method: "POST",
    url: `${apiUrl}/api/login`,
  });
};

const __register = async () => {
  return axios({
    method: "POST",
    url: `${apiUrl}/api/register`,
  });
};

function Login() {
  const REACT_APP_API_URL = process.env.REACT_APP_API_URL;
  const classes = useStyles();
  async function submit() {
    try {
      const res = await __login();
      const authenticate_url = res.data.authenticate_url;
      console.log("authenticate_url", authenticate_url);
      window.location.href = authenticate_url;
    } catch (exception) {
      alert(`Error: ${exception.message}`);
    }
  }
  async function register() {
    try {
      const res = await __register();
      console.log("res.data", res.data);
      window.location.href = res.data.registration_endpoint;
    } catch (exception) {
      alert(`Error: ${exception.message}`);
    }
  }
  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <Paper elevation={3} className={classes.paper}>
        <Avatar className={classes.avatar}>
          <i className="fad fa-hand-holding-seedling" />
        </Avatar>
        <Typography component="h1" variant="h5">
          Welcome
        </Typography>
        <form className={classes.form} noValidate>
          <Button
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            onClick={submit}
          >
            Login
          </Button>
          <Grid container>
            <Grid item xs>
              <Button
                variant="text"
                className={classes.link}
                onClick={register}
              >
                Register
              </Button>
            </Grid>
          </Grid>
        </form>
        <Box mt={8}>
          <Copyright />
        </Box>
      </Paper>
    </Container>
  );
}

export default Login;
