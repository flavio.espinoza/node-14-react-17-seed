const log = require('ololog');
const _ = require('lodash');
const { _error } = require('./_error');

const _isFormField = async (field) => {
  return new Promise((resolve) => {
    if (field.value && field.value !== '') {
      resolve({
        success: true,
        result: field.value,
      });
    } else {
      resolve({
        success: false,
        result: undefined,
      });
    }
  });
};

const _isDepartmentOf = async (value) => {
  return new Promise((resolve) => {
    let result = '';
    try {
      if (!value) {
        const err = new Error('No value provided for is_department_of');
        const error = _error({
          type: 'ingest_request_error',
          file: '_ingest_utils.js',
          method: '_isDepartmentOf',
          error: err,
          info: null,
        });
        resolve({
          success: false,
          result: '',
          error: error,
        });
      }
      if (typeof value === 'string' && value !== '') {
        result = value;
      }
      resolve({
        success: true,
        result: result,
      });
    } catch (err) {
      const error = _error({
        type: 'ingest_request_error_catch',
        file: '_ingest_utils.js',
        method: '_isDepartmentOf',
        error: err,
        info: null,
      });
      resolve({
        success: false,
        result: '',
        error: error,
      });
    }
  });
};

const _isPhoneNumber = async (form_data, form_props) => {
  return new Promise((resolve) => {
    const result = [];
    try {
      const _set = new Set(_.keys(form_props));
      const _keys = _.keys(form_data);
      _.each(_keys, (key) => {
        if (_set.has(key)) {
          const ado_data = form_props[key].ado_data;
          if (form_data[key].value && form_data[key].value !== '') {
            ado_data.value = form_data[key].value;
            const types = ado_data.type;
            const ado_data_types = [];
            _.each(types, (type) => {
              ado_data_types.push(`${type}`);
            });
            ado_data.type = ado_data_types;
            ado_data.use = []
            if (form_data[key].use_work === true || form_data[key].use_work === 'true') {
              ado_data.use.push(`PhoneNumber/Use/Work`);
            }
            if (form_data[key].use_home === true || form_data[key].use_home === 'true') {
              ado_data.use.push(`PhoneNumber/Use/Home`);
            }
            if (form_data[key].use_mobile === true || form_data[key].use_mobile === 'true') {
              ado_data.use.push(`PhoneNumber/Use/Mobile`);
            }
            if (form_data[key].use_fax === true || form_data[key].use_fax === 'true') {
              ado_data.use.push(`PhoneNumber/Use/Fax`);
            }
            if (form_data[key].use_pager === true || form_data[key].use_pager === 'true') {
              ado_data.use.push(`PhoneNumber/Use/Pager`);
            }
            if (form_data[key].use_other === true || form_data[key].use_other === 'true') {
              ado_data.use.push(`PhoneNumber/Use/Other`);
            }
            if (form_data[key].startDate) {
              ado_data.startDate = form_data[key].startDate;
            } else {
              delete ado_data.startDate;
            }
            if (form_data[key].endDate) {
              ado_data.endDate = form_data[key].endDate;
            } else {
              delete ado_data.endDate;
            }
            result.push(ado_data);
          }
        }
      });
      if (result.length > 0) {
        resolve({
          success: true,
          result: result,
        });
      } else {
        const err = new Error('No phone_numbers provided');
        const error = _error({
          type: 'ingest_request_error',
          file: '_ingest_utils.js',
          method: '_isPhoneNumber',
          error: err,
          info: null,
        });
        resolve({
          success: false,
          result: undefined,
          error: error,
        });
      }
    } catch (err) {
      const error = _error({
        type: 'ingest_request_error_catch',
        file: '_ingest_utils.js',
        method: '_isPhoneNumber',
        error: err,
        info: null,
      });
      resolve({
        success: false,
        result: undefined,
        error: error,
      });
    }
  });
};

const _isEmail = async (form_data, form_props) => {
  return new Promise((resolve) => {
    const result = [];
    try {
      const _set = new Set(_.keys(form_props));
      const _keys = _.keys(form_data);
      _.each(_keys, (key) => {
        if (_set.has(key)) {
          if (form_data[key].value && form_data[key].value !== '') {
            const ado_data = {
              type: `${form_props[key].ado_data.type}`,
              value: form_data[key].value,
            };
            if (form_data[key].use && form_data[key].use.length > 0) ado_data.use = form_data[key].use;
            if (form_data[key].country && form_data[key].country !== '') ado_data.country = form_data[key].country;
            if (form_data[key].domain && form_data[key].domain !== '') ado_data.domain = form_data[key].domain;
            if (form_data[key].startDate && form_data[key].startDate !== '')
              ado_data.startDate = form_data[key].startDate;
            if (form_data[key].endDate && form_data[key].endDate !== '') ado_data.endDate = form_data[key].endDate;
            result.push(ado_data);
          }
        }
      });
      if (result.length > 0) {
        resolve({
          success: true,
          result: result,
        });
      } else {
        const err = new Error('No social provided');
        const error = _error({
          type: 'ingest_request_error',
          file: '_ingest_utils.js',
          method: '_isEmail',
          error: err,
          info: null,
        });
        resolve({
          success: false,
          result: undefined,
          error: error,
        });
      }
    } catch (err) {
      const error = _error({
        type: 'ingest_request_error_catch',
        file: '_ingest_utils.js',
        method: '_isEmail',
        error: err,
        info: null,
      });
      resolve({
        success: false,
        result: undefined,
        error: error,
      });
    }
  });
};

const _isSocialHandle = async (form_data, form_props) => {
  return new Promise((resolve) => {
    const result = [];
    try {
      const _set = new Set(_.keys(form_props));
      const _keys = _.keys(form_data);
      _.each(_keys, (key) => {
        if (_set.has(key)) {
          const ado_data = form_props[key].ado_data;
          if (form_data[key].value && form_data[key].value !== '') {
            ado_data.handle = form_data[key].value;
            result.push(ado_data);
          }
        }
      });
      if (result.length > 0) {
        resolve({
          success: true,
          result: result,
        });
      } else {
        const err = new Error('no_social_handles');
        const error = _error({
          type: 'ingest_request_error',
          file: '_ingest_util.js',
          method: `_isSocialHandle`,
          error: err,
        });
        resolve({
          success: false,
          result: undefined,
          error: error,
        });
      }
    } catch (err) {
      const _err = new Error('catch_social_handles');
      const error = _error({
        type: 'ingest_request_error_catch',
        file: '_ingest_util.js',
        method: `_isSocialHandle`,
        error: _err,
      });
      resolve({
        success: false,
        result: undefined,
        error: error,
      });
    }
  });
};

const _isSocial = async (form_data, form_props) => {
  return new Promise((resolve) => {
    const result = [];
    try {
      const _set = new Set(_.keys(form_props));
      const _keys = _.keys(form_data);
      _.each(_keys, (key) => {
        if (_set.has(key)) {
          if (form_data[key].value && form_data[key].value !== '') {
            const ado_data = {
              type: `${form_props[key].ado_data.type}`,
              value: form_data[key].value,
            };
            if (form_data[key].use && form_data[key].use.length > 0) ado_data.use = form_data[key].use;
            if (form_data[key].country && form_data[key].country !== '') ado_data.country = form_data[key].country;
            if (form_data[key].domain && form_data[key].domain !== '') ado_data.domain = form_data[key].domain;
            if (form_data[key].startDate && form_data[key].startDate !== '')
              ado_data.startDate = form_data[key].startDate;
            if (form_data[key].endDate && form_data[key].endDate !== '') ado_data.endDate = form_data[key].endDate;
            result.push(ado_data);
          }
        }
      });
      if (result.length > 0) {
        resolve({
          success: true,
          result: result,
        });
      } else {
        const err = new Error('No social provided');
        const error = _error({
          type: 'ingest_request_error',
          file: '_ingest_util.js',
          method: `_isSocial`,
          error: err,
        });
        resolve({
          success: false,
          result: undefined,
          error: error,
        });
      }
    } catch (err) {
      const error = _error({
        type: 'ingest_request_error_catch',
        file: '_ingest_util.js',
        method: `_isSocial`,
        error: err,
      });
      resolve({
        success: false,
        result: undefined,
        error: error,
      });
    }
  });
};

const _isIdDocOrTaxId = async (form_data, form_props) => {
  return new Promise((resolve) => {
    const result = [];
    try {
      const _set = new Set(_.keys(form_props));
      const _keys = _.keys(form_data);
      _.each(_keys, (key) => {
        if (_set.has(key)) {
          if (form_data[key].value && form_data[key].value !== '') {
            const ado_data = {
              type: `${form_props[key].ado_data.type}`,
              value: form_data[key].value,
              ado_prop: form_props[key].form_props.name,
            };
            if (form_data[key].use && form_data[key].use.length > 0) ado_data.use = form_data[key].use;
            if (form_data[key].country && form_data[key].country !== '') ado_data.country = form_data[key].country;
            if (form_data[key].domain && form_data[key].domain !== '') ado_data.domain = form_data[key].domain;
            if (form_data[key].startDate && form_data[key].startDate !== '')
              ado_data.startDate = form_data[key].startDate;
            if (form_data[key].endDate && form_data[key].endDate !== '') ado_data.endDate = form_data[key].endDate;
            result.push(ado_data);
          }
        }
      });
      if (result.length > 0) {
        const id_obj = {};
        _.each(result, (obj) => {
          const ado_prop = obj.ado_prop;
          delete obj.ado_prop;
          id_obj[ado_prop] = [obj];
        });
        resolve({
          success: true,
          result: id_obj,
        });
      } else {
        const err = new Error('No id_docs or tax_ids provided');
        const error = _error({
          type: 'ingest_request_error',
          file: '_ingest_util.js',
          method: `_isIdDocOrTaxId`,
          error: err,
        });
        resolve({
          success: false,
          result: undefined,
          error: error,
        });
      }
    } catch (err) {
      const error = _error({
        type: 'ingest_request_error_catch',
        file: '_ingest_util.js',
        method: `_isIdDocOrTaxId`,
        error: err,
      });
      resolve({
        success: false,
        result: undefined,
        error: error,
      });
    }
  });
};

const _isPersonName = (form_data, form_props) => {
  return new Promise((resolve) => {
    const result = [];
    try {
      const _set = new Set(_.keys(form_props));
      const _keys = _.keys(form_data);
      _.each(_keys, (key) => {
        if (_set.has(key)) {
          if (form_data[key].value && form_data[key].value !== '') {
            const ado_data = {
              type: `${form_props[key].ado_data.type}`,
              value: form_data[key].value,
              ado_prop: form_props[key].ado_prop,
            };
            result.push(ado_data);
          }
        }
      });
      if (result.length > 0) {
        const names_arr = [];
        const names_obj = {};
        _.each(result, (obj) => {
          const ado_prop = obj.ado_prop;
          names_obj['type'] = `Name`;
          names_obj[ado_prop] = obj.value;
        });
        names_arr.push(names_obj);
        resolve({
          success: true,
          result: names_arr,
        });
      } else {
        const err = new Error('No person_names provided');
        const error = _error({
          type: 'ingest_request_error',
          file: '_ingest_util.js',
          method: `_isPersonName`,
          error: err,
        });
        resolve({
          success: false,
          result: undefined,
          error: error,
        });
      }
    } catch (err) {
      const error = _error({
        type: 'ingest_request_error_catch',
        file: '_ingest_util.js',
        method: `_isPersonName`,
        error: err,
      });
      resolve({
        success: false,
        result: undefined,
        error: error,
      });
    }
  });
};

const _isPostalAddress = async (form_data, form_props) => {
  return new Promise((resolve) => {
    const ado_data = {
      type: 'PostalAddress',
    };
    try {
      let _keys = _.keys(form_props);
      const index = _.findIndex(_keys, function(key) {
        return !form_data[key].value || form_data[key].value === '';
      });
      if (index < 0) {
        ado_data.streetAddress = form_data['street_address'].value;
        ado_data.addressLocality = form_data['city'].value;
        ado_data.addressRegion = form_data['state'].value;
        ado_data.postalCode = form_data['zip_code'].value;
        ado_data.addressCountry = form_data['country_code'].value;
        ado_data.fullAddress = `${form_data['street_address'].value}, ${form_data['city'].value}, ${form_data['state'].value}, ${form_data['zip_code'].value}, ${form_data['country_code'].value}`;
        resolve({
          success: true,
          result: [ado_data],
        });
      } else {
        const err = new Error(`The required address field is missing: ${_keys[index]}`);
        const error = _error({
          type: 'ingest_request_error',
          file: '_ingest_util.js',
          method: `_isPostalAddress`,
          error: err,
        });
        resolve({
          success: false,
          result: undefined,
          error: error,
        });
      }
    } catch (err) {
      const error = _error({
        type: 'ingest_request_error_catch',
        file: '_ingest_util.js',
        method: `_isPostalAddress`,
        error: err,
      });
      resolve({
        success: false,
        result: undefined,
        error: error,
      });
    }
  });
};

/**
 * @description List of phone use types options
 * */
const _phoneUseType = (parent_prop) => {
  /**
   * Phone Use Types
   * @description: PingDirectory default phone_number types
   * @param {{type:array$}} Settings to control the type (designation) applied to a user phone_number attribute
   * */
  const types = [
    'work',
    'home', 
    'mobile', 
    'fax', 
    'pager', 
    'other', 
  ];

  return {
    claim_question: {
      question_type: 'select-multiple',
      text: 'What is this number used for?',
    },
    claim_value_set: {
      id: ':cvs_type_use',
      value: [
        {
          id: 'PhoneNumber/Use/Work',
          display_id: 'use_work',
          type: 'work',
          text: 'Work',
          parent_prop: parent_prop,
          display_order: 0,
        },
        {
          id: 'PhoneNumber/Use/Home',
          display_id: 'use_home',
          type: 'home',
          text: 'Home',
          parent_prop: parent_prop,
          display_order: 1,
        },
        {
          id: 'PhoneNumber/Use/Mobile',
          display_id: 'use_mobile',
          type: 'Mobile',
          text: 'mobile',
          parent_prop: parent_prop,
          display_order: 2,
        },
        {
          id: 'PhoneNumber/Use/Fax',
          display_id: 'use_fax',
          type: 'fax',
          text: 'Fax',
          parent_prop: parent_prop,
          display_order: 3,
        },
        {
          id: 'PhoneNumber/Use/Pager',
          display_id: 'use_pager',
          type: 'pager',
          text: 'Pager',
          parent_prop: parent_prop,
          display_order: 3,
        },
        {
          id: 'PhoneNumber/Use/Other',
          display_id: 'use_other',
          type: 'other',
          text: 'Other',
          parent_prop: parent_prop,
          display_order: 3,
        },
      ],
    },
  };
};

/**
 * @description Resolver for ado.organization.addresses for populating front-end org_form, person_form, org_info_card and person_info_card
 * */
const _adoResolvePostalAddress = async (ado_data) => {
  return new Promise((resolve) => {
    try {
      if (ado_data.value && ado_data.value !== '') {
        const arr = ado_data.value.split(', ');
        resolve({
          success: true,
          result: {
            street_address: {
              value: arr[0],
            },
            city: {
              value: arr[1],
            },
            state: {
              value: arr[2],
            },
            zip_code: {
              value: arr[3],
            },
            country_code: {
              value: arr[4],
            },
          },
        });
      } else {
        resolve({
          success: true,
          result: {
            street_address: {
              value: '',
            },
            city: {
              value: '',
            },
            state: {
              value: '',
            },
            zip_code: {
              value: '',
            },
            country_code: {
              value: '',
            },
          },
        });
      }
    } catch (err) {
      const error = _error({
        type: 'ingest_request_error_catch',
        file: '_ingest_util.js',
        method: `_adoResolvePostalAddress`,
        error: err,
      });
      resolve({
        success: false,
        result: {
          street_address: {
            value: '',
          },
          city: {
            value: '',
          },
          state: {
            value: '',
          },
          zip_code: {
            value: '',
          },
          country_code: {
            value: '',
          },
        },
        error: error,
      });
    }
  });
};

module.exports = {
  _isFormField,
  _isDepartmentOf,
  _isPhoneNumber,
  _isEmail,
  _isSocialHandle,
  _isSocial,
  _isIdDocOrTaxId,
  _isPersonName,
  _isPostalAddress,
  _phoneUseType,
  _adoResolvePostalAddress,
};
